package mysqltestutil

import (
	"context"
	"database/sql"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"testing"
	"time"

	"github.com/golang/glog"
)

// InitDaemon sets up the MySQL daemon for use in tests.
//
// Call it from TestMain.
//
// mysqld is the path to the mysqld binary, mysqlBasedir is the path to the
// base directory for other mysql data, and mysqlPort is the port that will be
// used for the temporary MySQL instance.
//
// mysqlPort must be specified.
//
// If mysqld is empty, then both mysqld and mysqlBasedir will be chosen from
// some candidates; e.g. "/usr/sbin/mysqld" + "/usr" and
// "/usr/local/mysql/bin/mysqld" + "/usr/local/mysql". A simple check, such as
// os.Stat, will be used to verify if the mysqld is present.
func InitDaemon(m *testing.M, mysqld, mysqlBasedir string, mysqlPort int, databaseCreate, schemaCreate SchemaSetupFunc, databaseName, tempdirBasename string) (int, context.CancelFunc) {
	// Do not use fatal in this function.
	// Doing so would skip defers, which are used for cleanup.

	var cancel context.CancelFunc
	var removeAll context.CancelFunc
	realCancel := func() {
		if cancel != nil {
			cancel()
		}
		if removeAll != nil {
			removeAll()
		}

	}

	tempdir, err := ioutil.TempDir("", tempdirBasename)
	if err != nil {
		glog.Errorln(err)
		return 16, realCancel
	}
	removeAll = func() { os.RemoveAll(tempdir) }
	for _, subdir := range []string{"tmp"} {
		err := os.Mkdir(tempdir+"/"+subdir, os.FileMode(0700))
		if err != nil {
			glog.Errorln(err)
			return 16, realCancel
		}
	}

	if mysqld == "" {
		candidates := []struct{ binary, basedir string }{
			{binary: "/usr/sbin/mysqld", basedir: "/usr"},
			{binary: "/usr/local/mysql/bin/mysqld", basedir: "/usr/local/mysql"},
		}
		for _, candidate := range candidates {
			if _, err := os.Stat(candidate.binary); err == nil { // || !os.IsNotExist(err) {
				mysqld = candidate.binary
				mysqlBasedir = candidate.basedir
				break
			}
		}
	}
	if mysqlPort == 0 {
		mysqlPort = 9256
	}

	ioutil.WriteFile(tempdir+"/my.cnf", []byte(fmt.Sprintf(`
[client]
port = %d

[mysqld]
port = %d
socket = 
basedir = %s
tmpdir = `+tempdir+`/tmp
datadir = `+tempdir+`/data
pid-file = `+tempdir+`/mysqld.pid
# default 128mb is too big for tests.
innodb_buffer_pool_size = 16M
`, mysqlPort, mysqlPort, mysqlBasedir)),
		os.FileMode(0600))

	cmd := exec.Command(mysqld, "--defaults-file="+tempdir+"/my.cnf", "--datadir="+tempdir+"/data", "--log-error-verbosity=3", "--initialize-insecure")
	stdout, err := cmd.Output()
	if err != nil {
		glog.Infoln(string(stdout))
		out := ""
		if exerr, ok := err.(*exec.ExitError); ok {
			out = string(exerr.Stderr)
		}
		glog.Infof("initializing mysql: %s (%s)", err.Error(), out)
		return 16, realCancel
	}

	var ctx context.Context
	ctx, cancel = context.WithCancel(context.Background())

	cmd = exec.CommandContext(ctx, mysqld, "--defaults-file="+tempdir+"/my.cnf", "--general_log=1", "--general_log_file="+tempdir+"/mysql_log.txt")
	stderr, err := cmd.StderrPipe()
	if err != nil {
		glog.Errorf("cannot get stderr pipe: %s", err)
		return 16, realCancel
	}
	cmd.Start()
	for _, sleepDelay := range []int32{1, 1, 1} {
		err = dbInit(databaseCreate, schemaCreate, databaseName, mysqlPort)
		if err == nil {
			break
		}
		if _, ok := err.(*SchemaSetupError); ok {
			break
		}
		glog.Warningf("retrying dbInit() with sleep: %d", sleepDelay)
		time.Sleep(time.Duration(sleepDelay) * time.Second)
	}
	if err != nil {
		glog.Errorf("error initializing mysqld: %s", err)

		// stop mysql
		cancel()
		cancel = nil

		slurp, err := ioutil.ReadAll(stderr)
		if err != nil {
			glog.Errorf("cannot read stderr pipe: %s", err)
			return 16, nil
		}

		glog.Infof("%s", slurp)
		return 16, realCancel
	}

	return 0, realCancel
}

type SchemaSetupFunc func(*sql.DB) error

type SchemaSetupError struct {
	Query string
	Err   error
}

func (e SchemaSetupError) Error() string {
	return fmt.Sprintf("executing init query %s failed: %s", e.Query, e.Err.Error())
}

func dbInit(databaseCreate, schemaCreate SchemaSetupFunc, databaseName string, mysqlPort int) error {
	db, err := sql.Open("mysql", fmt.Sprintf("root:@tcp(localhost:%d)/", mysqlPort)) // TODO(ivucica): how to timeout?
	defer db.Close()
	if err != nil {
		return err
	}
	err = db.Ping()
	if err != nil {
		return err
	}

	if err := databaseCreate(db); err != nil {
		glog.Infof("error dbcreate : %v", err)
		return err
	}

	// reconnect to the correct, now newly created database
	db, err = sql.Open("mysql", fmt.Sprintf("root:@tcp(localhost:%d)/%s?sql_mode=ANSI_QUOTES&parseTime=true", mysqlPort, databaseName)) // TODO(ivucica): how to timeout?
	defer db.Close()
	if err != nil {
		return err
	}
	err = db.Ping()
	if err != nil {
		return err
	}

	if err := schemaCreate(db); err != nil {
		return err
	}

	return nil
}
